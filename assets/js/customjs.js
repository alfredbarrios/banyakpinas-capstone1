		/*Collapse Menu on Click*/
		$('.nav-link').on('click', function() {
		    $('.navbar-collapse').collapse('hide');
		});

		/*Higlight Menu on Click*/
		$('#bp_Nav ul li a').on('click', function() {
		    $('#bp_Nav ul li a.currentmenu').removeClass('currentmenu');
		    $(this).addClass('currentmenu');
		    $('#bp_popup_instructions a').popover('hide');
		});

		/*loading Screen*/
		$(window).on("load", function() {
		    // Animate loader off screen
		    $(".loader_container").fadeOut("slow");
		});

		/*Random Quote Generator*/
		$(document).ready(function() {

		    function getQuote() {
		        var quotes = ["To stop or to keep going? The one who decides is yourself.", "Working as a team is far better than working individually.", "Your weakness cant be overcome with knowledge or technique. So climb. Keep climbing until your legs stop moving.", "The most important part of fighting a champion is to never hesitate.To pursue the methods you believe in without doubting yourself.", "If you dont move forward, youll be left behind.", "Problems that cannot be solved do not exist in this world.", "Dreams start by believing.", "Its a programmers job to make the most of limited resources to turn an impractical idea into reality."];
		        var author = ["Shoukichi Naruko", "Shoukichi Naruko", "Yuusuke Makishima", "Kinjou Shingo", "Kinjou Shingo", "Yuki Nagato", "Haruhi Suzumiya", "Akasaka Ryuunosuke"];

		        var randomNum = Math.floor((Math.random() * quotes.length));
		        var randomQuote = quotes[randomNum];
		        var randomAuthor = author[randomNum];

		        $(".author").html("<strong>" + randomAuthor + "</strong>");
		        $(".quote").text(randomQuote);

		    }

		    if ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) && ($(window).width() < 767)) {

		    } else {
		        window.setInterval(function() {
		            getQuote();
		        }, 6000);
		    }

		    /*Popover*/
		    if ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) && ($(window).width() < 991)) {

		    } else {

		        /*Popover via time*/
		        setTimeout(function() {
		            $('#bp_popup_instructions1 a').popover('show');
		        }, 3000);

		        setTimeout(function() {
		            $('#bp_popup_instructions a').popover('hide');
		            $('#bp_popup_instructions1 a').popover('hide');
		        }, 20000);

		        /*Popover after modal click for index page*/

		        $('#Landing_Page_Modal_Backdrop_button').on('click', function() {
		            $('#bp_popup_instructions a').popover('show');
		        });

		        $('#Landing_Page_Modal_Backdrop_close').on('click', function() {
		            $('#bp_popup_instructions a').popover('show');
		        });

		        /*Remove Popover on hover*/
		        /*Hide Pop up on Hover*/
		        $('#bp_Nav ul li a').hover(function() {
		            $('#bp_popup_instructions a').popover('hide');
		            $('#bp_popup_instructions1 a').popover('hide');
		        });

		    }

		});